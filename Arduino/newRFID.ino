
#include <SPI.h>
#include <MFRC522.h>
#include <WiFiNINA.h>

#define RST_PIN         9          // Configurable, see typical pin layout above
#define SS_PIN          10         // Configurable, see typical pin layout above

MFRC522 mfrc522(SS_PIN, RST_PIN);  // Create MFRC522 instance
WiFiSSLClient client;

char ssid[] = "Livebox-ECC4";
char pass[] = "morganitos";
char host[] = "script.google.com";  
String url = "/macros/s/AKfycbwU3ySb1S7g4FPogvAkoOZEpVKsfzQibc2yWZq2u_rTLd0ZfiZj/exec";
int port = 443;
int status = WL_IDLE_STATUS;

void setup() {
  Serial.begin(9600);   // Initialize serial communications with the PC
  while (!Serial);    // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)

  SPI.begin();      // Init SPI bus
  mfrc522.PCD_Init();   // Init MFRC522
  delay(4);       // Optional delay. Some board do need more time after init to be ready, see Readme
  mfrc522.PCD_DumpVersionToSerial();  // Show details of PCD - MFRC522 Card Reader details
  
  // check for the WiFi module:
  if (WiFi.status() == WL_NO_MODULE) {
    Serial.println("Communication with WiFi module failed!");
    while (true);
  }

  String fv = WiFi.firmwareVersion();
  if (fv < "1.0.0") {
    Serial.println("Please upgrade the firmware");
  }

 // attempt to connect to WiFi network:
  while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
    status = WiFi.begin(ssid, pass);
    // wait 10 seconds for connection:
    delay(10000);
  }
  Serial.println("Connected to wifi");
  printWiFiStatus();

  Serial.println("\nStarting connection to server...");
  // if you get a connection, report back via serial:
  if (client.connect(host, port)) {
    Serial.println("connected to server");
  }
  
}

void loop() {
  // Reset the loop if no new card present on the sensor/reader. This saves the entire process when idle.
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    return;
  }

  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return;
  }
  mfrc522.PICC_HaltA();

  char temp[10];
  char base[] = "0123456789ABCDEF";
  for (byte i = 0; i < mfrc522.uid.size; i++) {
  temp[2*i] = base[mfrc522.uid.uidByte[i] / 0x10];
  temp[2*i + 1] = base[mfrc522.uid.uidByte[i]  % 0x10]; 
  }
  temp[9] = 0;
  String _uid = String(temp);
  
  Serial.println("Card UID: " + _uid);
  
  // Make a HTTP request:
  String postRequest = "GET "+ url + "?card=" + _uid;
  client.print(postRequest);
  client.println(" HTTP/1.1");
  client.println("Host: script.google.com");
  client.println("Connection: Keep-Alive");
  client.println();
}


void printWiFiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your board's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}
